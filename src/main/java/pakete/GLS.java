package pakete;

import pakete.NotDeliverableException;

import java.util.Arrays;

public class GLS
{
  public int getPriceForDimensions(int length, int width, int height, int weight)
  {
    checkDimension(length, "Length");
    checkDimension(width, "Width");
    checkDimension(height, "Height");
    checkDimension(weight, "Weight");

    int[] dimensions = new int[]{length, width, height};
    Arrays.sort(dimensions);
    int shortestSide = dimensions[0];
    int longestSide = dimensions[2];
    int size = shortestSide + longestSide;

    return getPriceForDimensions(size, weight);
  }

  int getPriceForDimensions(int size, int weight)
  {
    if (weight > 40000) throw new NotDeliverableException();

    if (size <= 35) return 350;
    else if (size <= 50) return 430;
    else if (size <= 70) return 540;
    else if (size <= 90) return 950;
    else throw new NotDeliverableException();
  }
  private static void checkDimension(int dimension, String dimensionName)
  {
    if (dimension <= 0) throw new NotDeliverableException(dimensionName + " must be bigger than 0");
  }
}
